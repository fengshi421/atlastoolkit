% =========================================================================
%                              Atlas Construction Toolkit
% =========================================================================
06/30/2014, first version.
For comments or questions please turn to Feng Shi (fengshiunc@gmail.com).

The Atlas Construction Toolkit aims to constuct atlas from a group of aligned images using sparse representation technique.
It is an implementation of below paper:
"Neonatal Atlas Construction Using Sparse Representation”, First published online, Human Brain Mapping, 2014. [Feng Shi, Li Wang, Guorong Wu, Gang Li, John H. Gilmore, Weili Lin, Dinggang Shen]

In this toolbox (bin folder) you can find the following files:
================================================

1. fMain - the main file in this toolbox that load data and call the core function for processing.
2. fAtlasSparse - the core function to implement the atlas construction.
  
Required software
================================================
This toolkit require below toolbox (placed at bin folder). Please cd to the folder and compile it before use.

1. http://spams-devel.gforge.inria.fr/

Prepare data
================================================

1. You input data should be already aligned together. If not, please use http://www.nitrc.org/projects/glirt/ for this purpose. It takes either segmented images or intensity images as inputs. 
2. Including of segmented images could improve the performance. Here please assign the voxel value of GM to 150, WM to 250, and CSF to 10 in segmented images.
3. If having no segmented images, for pediatric images you may consider to use iBEAT software (http://www.nitrc.org/projects/ibeat/) to generate them, for adult images FAST (http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FAST) or SPM (http://www.fil.ion.ucl.ac.uk/spm/) could be used.
3. Results include the generated atlases for intensity images as well as GM/WM/CSF probablity maps.

To run the toolbox
================================================

1. move to the bin folder using cd command. (if not, please adjust the addpath for running program)
2. run the fMain to see the example of using testdata as input for atlas construction.
3. param.patchmoveoverlap could be set equal to patchsize when testing for faster speed, while set it to 1 is recommended for best performance. In my computer, it took 40 mins for the testdata when set param.patchmoveoverlap=1, while it took 12 mins when param.patchmoveoverlap=3.




