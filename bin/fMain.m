%==========================================================================
%   P E R F O R M   A T L A S   C O N S T R U C T I O N
%       U S I N G  S P A R S E   R E P R E S E N T A T I O N
%==========================================================================
% This is the main program
% This program will use the images in testdata folder, and contruct atlas
% 06/30/2014, first version.
% For comments or questions please turn to Feng Shi (fengshiunc@gmail.com).
% =========================================================================

%% include the path
% cd to the program path, and run this program. Or you may need to revise
% below address to their actual path
addpath ../bin
addpath ../bin/NIfTI_20140122

%% data file folder
% all images should be registered into the same space
% if not, below software is recommended to use for this purpose
% http://www.nitrc.org/projects/glirt/
% After that, all images (intensity and seg) should have the same size
% segment image should assign GM to 150, WM to 250, and CSF to 10
% PS, if have no segmented images, for pediatric images you may consider to
% use iBEAT software (http://www.nitrc.org/projects/ibeat/) to generate them, 
% for adult images FAST (http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FAST) or SPM 
% (http://www.fil.ion.ucl.ac.uk/spm/) could be used.

fileDir='../testdata/';
fileseg = '-neo-T2-seg';
fileintensity = '-neo-T2';

subjlist = dir([fileDir,'*',fileintensity,'.nii.gz']);
subjlistSeg = dir([fileDir,'*',fileseg,'.nii.gz']);

% output dir
outDir = [fileDir,'atlas/'];
mkdir(outDir);

%% parameter
% options for parallel computing. Program will connect local matlabpool workers of bellow numbers.
param.parallel = 6;

% options for SPAMS
param.mode=2;
param.lambda = 0.05;    % now lambda has the same meaning.
param.lambda2 = 0;  % so that this cost function is as same as that one used in LEAP
param.pos=1;
param.numThreads=1;%-1; % uses all the cores of the machine

% suggested parameter: patchsize=6, topk=10, patchmoveoverlap=1 (need hours to run when set as 1 for best performance)
param.patchsize = 6;
param.topk = 10;
param.patchmoveoverlap = param.patchsize; %1;
disp(['patchsize topk overlap are ',num2str(param.patchsize),', ',num2str(param.topk),', ',num2str(param.patchmoveoverlap)]);


%% make a mean image first
outfile = [outDir,'meanimg.nii.gz'];
meanimg = fAtlasMean(fileDir,subjlist,outfile);
imagesize = size(meanimg);

%% image crop
[xmin,xmax,ymin,ymax,zmin,zmax] = boundingBox(meanimg);
patchsize = param.patchsize;

xmin = max(xmin-patchsize*2,1);
xmax = min(xmax+patchsize*2,imagesize(1));
ymin = max(ymin-patchsize*2,1);
ymax = min(ymax+patchsize*2,imagesize(2));
zmin = max(zmin-patchsize*2,1);
zmax = min(zmax+patchsize*2,imagesize(3));
meanimgCrop = meanimg(xmin:xmax,ymin:ymax,zmin:zmax);

imagesizecrop = size(meanimgCrop);

%% load all images
matrix = zeros(imagesizecrop(1),imagesizecrop(2),imagesizecrop(3),length(subjlist));
matrixSeg = zeros(imagesizecrop(1),imagesizecrop(2),imagesizecrop(3),length(subjlistSeg));
for i = 1:length(subjlist)
    disp(i);
    nii = load_nii([fileDir,subjlist(i).name]);
    tmp = nii.img;
    matrix(:,:,:,i) = tmp(xmin:xmax,ymin:ymax,zmin:zmax);
    nii = load_nii([fileDir,subjlistSeg(i).name]);
    tmp = nii.img;
    matrixSeg(:,:,:,i) = tmp(xmin:xmax,ymin:ymax,zmin:zmax);
end

%% atlas construction using sparse representation
% outputs are atlases for intensity images and segment images
% inputs and outputs are the cropped images.
tic
[ressparsityCrop,ressparsitySegCrop,ressparsitySegGMCrop,ressparsitySegWMCrop,ressparsitySegCSFCrop] = fAtlasSparse(meanimgCrop,matrix,matrixSeg,param);
toc/60
% clear matrix matrixSeg;

%% crop back
ressparsity = zeros(imagesize);
ressparsitySeg = zeros(imagesize);
ressparsitySegGM = zeros(imagesize);
ressparsitySegWM = zeros(imagesize);
ressparsitySegCSF = zeros(imagesize);

ressparsity(xmin:xmax,ymin:ymax,zmin:zmax) = ressparsityCrop;
ressparsitySeg(xmin:xmax,ymin:ymax,zmin:zmax) = ressparsitySegCrop;
ressparsitySegGM(xmin:xmax,ymin:ymax,zmin:zmax) = ressparsitySegGMCrop;
ressparsitySegWM(xmin:xmax,ymin:ymax,zmin:zmax) = ressparsitySegWMCrop;
ressparsitySegCSF(xmin:xmax,ymin:ymax,zmin:zmax) = ressparsitySegCSFCrop;

% clear ressparsityCrop ressparsitySegCrop ressparsitySegGMCrop ressparsitySegWMCrop ressparsitySegCSFCrop;

%% show result
slice = 120;
figure;imshow(meanimg(:,:,slice),[]);
figure;imshow(ressparsity(:,:,slice),[]);
figure;imshow(ressparsitySegGM(:,:,slice),[]);
figure;imshow(ressparsitySegWM(:,:,slice),[]);
figure;imshow(ressparsitySegCSF(:,:,slice),[]);

%% % save image
prefix = 'resSparse';

% save results
nii = load_nii([outDir,'meanimg.nii.gz']);
nii.hdr.dime.datatype = 16;nii.hdr.dime.bitpix = 32; % save out float type

outfile=[outDir,prefix,'-patchsize_',num2str(param.patchsize),'-topk_',num2str(param.topk),'-move_',num2str(param.patchmoveoverlap),'.nii.gz'];
nii.img = ressparsity;
save_nii(nii,outfile);

outfile=[outDir,prefix,'-GM-patchsize_',num2str(param.patchsize),'-topk_',num2str(param.topk),'-move_',num2str(param.patchmoveoverlap),'.nii.gz'];
nii.img = ressparsitySegGM;
save_nii(nii,outfile);

outfile=[outDir,prefix,'-WM-patchsize_',num2str(param.patchsize),'-topk_',num2str(param.topk),'-move_',num2str(param.patchmoveoverlap),'.nii.gz'];
nii.img = ressparsitySegWM;
save_nii(nii,outfile);

outfile=[outDir,prefix,'-CSF-patchsize_',num2str(param.patchsize),'-topk_',num2str(param.topk),'-move_',num2str(param.patchmoveoverlap),'.nii.gz'];
nii.img = ressparsitySegCSF;
save_nii(nii,outfile);

outfile=[outDir,prefix,'-seg-patchsize_',num2str(param.patchsize),'-topk_',num2str(param.topk),'-move_',num2str(param.patchmoveoverlap),'.nii.gz'];
nii.img = ressparsitySeg;
save_nii(nii,outfile);


