function [ressparsity,ressparsitySeg,ressparsitySegGM,ressparsitySegWM,ressparsitySegCSF] = fAtlasSparse(meanimg,matrix,matrixSeg,param)
% This is the mean program for atlas construction using sparse
% representation.
% 06/30/2014, first version.
% For comments or questions please turn to Feng Shi (fengshiunc@gmail.com).

% cd to the program path, and run this program. Or you may need to revise
% below address to their actual path

addpath(genpath('../bin/spams-matlab'));

% please revise below values according to the real values in your segmented images.
GMValue = 150;
WMValue = 250;
CSFValue = 10;

%% parameter
patchsize = param.patchsize;
patchmoveoverlap = param.patchmoveoverlap;
topk = param.topk;
subjNum = size(matrix,4);
P = patchsize^3;

%% load mean image
imagesize = size(meanimg);
imagesizecrop = imagesize;

%% for each patch, make the dict
ressparsity = zeros(imagesize);
ressparsitySeg = zeros(imagesize);
ressparsitySegGM = zeros(imagesize);
ressparsitySegWM = zeros(imagesize);
ressparsitySegCSF = zeros(imagesize);
ressparsityBG = zeros(imagesize);

eval(['matlabpool ',num2str(param.parallel)]);

for i = 1:patchmoveoverlap:imagesizecrop(1)-patchsize
    disp(['Total is ', num2str(imagesizecrop(1)-patchsize), ', now i is ----> ',num2str(i)]);  
    xnew = i:i+patchsize-1;
    for j = 1:patchmoveoverlap:imagesizecrop(2)-patchsize    
        ynew = j:j+patchsize-1;
        kTotal = 1:patchmoveoverlap:imagesizecrop(3)-patchsize;
        tmppatchTotal = zeros(length(kTotal),P); % slice
        tmpTotal = zeros(length(kTotal),P*subjNum);
        tmp2Total = zeros(length(kTotal),P*subjNum);
        for k = 1:length(kTotal) % 1:patchmoveoverlap:imagesizecrop(3)-patchsize
            znew = kTotal(k):kTotal(k)+patchsize-1;
            tmppatch = meanimg(xnew,ynew,znew);
            tmppatchTotal(k,:) = tmppatch(:);
            tmp = double(matrix(xnew,ynew,znew,:));
            tmpTotal(k,:) = tmp(:);            
            tmp2 = double(matrixSeg(xnew,ynew,znew,:));
            tmp2Total(k,:) = tmp2(:);
        end
        resTotal = zeros(length(kTotal),P*5);
        parfor k = 1:length(kTotal)
            tmppatch = tmppatchTotal(k,:);
            if sum(tmppatch) == 0 ;  continue ;  end
            tmp = reshape(tmpTotal(k,:),[P,subjNum]);
            tmp2 = reshape(tmp2Total(k,:),[P,subjNum]);
            tmpGM = zeros(size(tmp2));tmpGM(tmp2==GMValue) = 100;
            tmpWM = zeros(size(tmp2));tmpWM(tmp2==WMValue) = 100;
            tmpCSF = zeros(size(tmp2));tmpCSF(tmp2==CSFValue) = 100;
            matrixdict = [tmp ; tmpGM; tmpWM];
            % select most similar subjects
            %centerpatch = mean(matrixdict,2);
            centerpatch = [tmppatch(:); mean(tmpGM,2); mean(tmpWM,2)];
            % find k nearest subjects
            similarityInt = corr(matrixdict(1:patchsize^3,:),centerpatch(1:patchsize^3));
            similarityGM =corr(matrixdict(patchsize^3+1:2*patchsize^3,:),centerpatch(patchsize^3+1:2*patchsize^3));
            similarityWM = corr(matrixdict(2*patchsize^3+1:3*patchsize^3,:),centerpatch(2*patchsize^3+1:3*patchsize^3)) ;
            similarityInt(isnan(similarityInt)) = 0;
            similarityGM(isnan(similarityGM)) = 0;
            similarityWM(isnan(similarityWM)) = 0;
            similarity = 1/2 * similarityInt + 1/4 * similarityGM + 1/4 * similarityWM;
            [~, v] = sort(similarity,'descend');
            ind = v(1:topk);            
            A=repmat(matrixdict,[length(ind) 1]);
            y=matrixdict(:,ind); y = y(:);            
            % try the SPAMS
            x = full(mexLasso(y,A,param));
            
            % for boundaries
            if nnz(centerpatch==0)/length(centerpatch) > 0.3 %   
                x = ones(size(x))/length(x);
            end

            % make new patch
            resTotal(k,:) = [tmp * x; tmpGM * x ./100; tmpWM * x ./ 100; tmpCSF * x ./ 100; ones(P,1)];
        end
        for k = 1:length(kTotal) %1:patchmoveoverlap:imagesizecrop(3)-patchsize
            znew = kTotal(k):kTotal(k)+patchsize-1;
            ressparsity(xnew,ynew,znew) = ressparsity(xnew,ynew,znew) + reshape(resTotal(k,1:P),[patchsize patchsize patchsize]);
            ressparsitySegGM(xnew,ynew,znew) = ressparsitySegGM(xnew,ynew,znew) + reshape(resTotal(k,P+1:2*P),[patchsize patchsize patchsize]);
            ressparsitySegWM(xnew,ynew,znew) = ressparsitySegWM(xnew,ynew,znew) + reshape(resTotal(k,2*P+1:3*P),[patchsize patchsize patchsize]);
            ressparsitySegCSF(xnew,ynew,znew) = ressparsitySegCSF(xnew,ynew,znew) + reshape(resTotal(k,3*P+1:4*P),[patchsize patchsize patchsize]);
            ressparsityBG(xnew,ynew,znew) = ressparsityBG(xnew,ynew,znew) + 1;
        end
    end
end

matlabpool close;

% final seg
for i = 1:imagesize(1)
    for j = 1:imagesize(2)
        for k = 1:imagesize(3)
            a=[ressparsitySegCSF(i,j,k),ressparsitySegGM(i,j,k),ressparsitySegWM(i,j,k)];
            if sum(a) > 0.3
                [~,ind] = max(a);                
                if ind ==1; value = CSFValue;elseif ind ==2; value = GMValue; else value = WMValue;end;
                ressparsitySeg(i,j,k) = value;
            end
        end
    end
end


ressparsity(ressparsityBG>0) = ressparsity(ressparsityBG>0)./ressparsityBG(ressparsityBG>0);
ressparsitySegGM(ressparsityBG>0) = ressparsitySegGM(ressparsityBG>0)./ressparsityBG(ressparsityBG>0);
ressparsitySegWM(ressparsityBG>0) = ressparsitySegWM(ressparsityBG>0)./ressparsityBG(ressparsityBG>0);
ressparsitySegCSF(ressparsityBG>0) = ressparsitySegCSF(ressparsityBG>0)./ressparsityBG(ressparsityBG>0);

