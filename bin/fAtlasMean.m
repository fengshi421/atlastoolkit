function [meanimg,matrix] = fAtlasMean(fileDir,subjlist,outfile)
% input is the list of all images, and output file name
% output is the mean image

for i = 1:length(subjlist)
    disp(i);
    nii = load_nii([fileDir,subjlist(i).name]);
    matrix(:,:,:,i) = nii.img;
end

meanimg = mean(matrix,4);

% save mean
if nargin > 2    
    nii.img = meanimg;
    save_nii(nii,outfile);
end