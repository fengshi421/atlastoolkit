function [xmin,xmax,ymin,ymax,zmin,zmax] = boundingBox(img)


for i = 1:size(img,1)    
    slice = squeeze(img(i,:,:));
    if max(slice(:)) > 0
        xmin = i;
        break;
    end
end

for i = size(img,1):-1:1
    slice = squeeze(img(i,:,:));
    if max(slice(:)) > 0
        xmax = i;
        break;
    end
end

for i = 1:size(img,2)    
    slice = squeeze(img(:,i,:));
    if max(slice(:)) > 0
        ymin = i;
        break;
    end
end

for i = size(img,2):-1:1
    slice = squeeze(img(:,i,:));
    if max(slice(:)) > 0
        ymax = i;
        break;
    end
end

for i = 1:size(img,3)    
    slice = squeeze(img(:,:,i));
    if max(slice(:)) > 0
        zmin = i;
        break;
    end
end

for i = size(img,3):-1:1
    slice = squeeze(img(:,:,i));
    if max(slice(:)) > 0
        zmax = i;
        break;
    end
end

    

